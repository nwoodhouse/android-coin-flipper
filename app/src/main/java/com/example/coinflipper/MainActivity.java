package com.example.coinflipper;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // wire up the User interface
        Button button = this.findViewById(R.id.flip);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // roll a dice...
                boolean isHead = flipIsHead();
                updateUi(isHead);
            }
        });
    }

    private void updateUi(boolean isHead) {
        String message = isHead
                ? "Heads!"
                : "Tails!";

        TextView result = this.findViewById(R.id.result);
        result.setText(message);

        ImageView image = this.findViewById(R.id.image);
        if (isHead)
            image.setImageResource(R.drawable.farthing);
        else
            image.setImageResource(R.drawable.canadian);
    }

    private boolean flipIsHead()
    {
        return Math.random() > 0.5;
    }
}
